#!/bin/bash

# ===============================================================
# Author: pascalau@student.ethz.ch
#
# This script uses ffmpeg to quickly compress videos to a target
# filesize, which is useful for paper submissions. The script
# only has a hard dependency on ffmpeg and can therefore be run
# on headless servers/clusters to massivly boost transcode speed.
# Unfortunately euler does not support any of the codecs used
# in this script.
# ===============================================================

DURATION=0
TARGET_FILESIZE=20
TARGET_BITSTREAM=0
AUDIO_BITSTREAM=0
RESOLUTION="1920:1080"
PRESET="medium"
CODEC="x264"
HW_ACCEL=false
HW_ACCEL_TYPE=""
HELP=false
INPUT_FILES=()

while test $# -gt 0
do
    case $1 in
        -h|--help)
        HELP=true
        shift # Remove --initialize from processing
        continue
        ;;
        -i|--input)
        INPUT="$2"
        shift
        shift
        continue
        ;;
        -s|--size)
        TARGET_FILESIZE="$2"
        shift
        shift
        continue
        ;;
        -p|--preset)
        PRESET="$2"
        shift
        shift
        continue
        ;;
        -c|--codec)
        CODEC="$2"
        shift
        shift
        continue
        ;;
        -r|--resolution)
        RESOLUTION="$2"
        shift
        shift
        continue
        ;;
        -hw|--hw-accel)
        HW_ACCEL=true
        HW_ACCEL_TYPE="$2"
        shift
        shift
        continue
        ;;
        *)
        INPUT_FILES+=("$1")
        shift # Remove generic argument from processing
        continue
        ;;
    esac
done


if [ ${#INPUT_FILES[@]} -eq 0 ] | [ "$HELP" = true ] ; then
    echo "Script to compress videos with ffmpeg"
    echo ""
    echo "./compress_video.sh [options] inputfile [inputfile]"
    echo ""
    echo "options:"
    echo "-h, --help                show brief help"
    echo "-s, --size                targetsize of file in MB"
    echo "-p, --preset              preset of ffmpeg"
    echo "-c, --codec               video codec of output x265/x264"
    echo "-r, --resolution          resolution of output"
    exit 0
fi


for i in ${INPUT_FILES[@]} ; do
    DURATION=$(ffprobe -v error -show_streams -select_streams a "$i" | grep -Po "(?<=^duration\=)\d*")
    AUDIO_BITSTREAM=$(ffprobe -v error -pretty -show_streams -select_streams a "$i" | grep -Po "(?<=^bit_rate\=)\d*")
    TARGET_BITSTREAM=$(( ( ($TARGET_FILESIZE - 1) * 8192 / $DURATION ) - $AUDIO_BITSTREAM ))

    if [ "$HW_ACCEL" = false ] ; then
        ffmpeg -i "$i" -c:v lib"$CODEC" -preset "$PRESET" -tune "film" -vf \
            scale="$RESOLUTION" -b:v "$TARGET_BITSTREAM"k -"$CODEC"-params "pass=1" -an -f null /dev/null
        ffmpeg -i "$i" -c:v lib"$CODEC" -preset "$PRESET" -tune "film" -vf \
            scale="$RESOLUTION" -b:v "$TARGET_BITSTREAM"k -"$CODEC"-params "pass=2" compressed_"$i"

    else
    echo "Using HW acceleration"
        case ${HW_ACCEL_TYPE} in
            vaapi)
            RESOLUTION=(${RESOLUTION//x/ })
            echo "${RESOLUTION[0]}"
            echo "${RESOLUTION[1]}"
            FILTERS="deinterlace_vaapi=rate=field:auto=1,hwupload,scale_vaapi=w=${RESOLUTION[0]}:h=${RESOLUTION[1]}:format=nv12"
            echo "$FILTERS"
            ffmpeg -hwaccel vaapi -hwaccel_output_format vaapi \
                -vaapi_device /dev/dri/renderD128 -i "$i" \
                -vf "$FILTERS" \
                -c:v hevc_vaapi -b:v "$TARGET_BITSTREAM"k -pass 1 -an -f null -
            ffmpeg -hwaccel vaapi -hwaccel_output_format vaapi \
                -vaapi_device /dev/dri/renderD128 -i "$i" \
                -vf "$FILTERS" \
                -c:v hevc_vaapi -b:v "$TARGET_BITSTREAM"k -pass 2 compressed_"$i"
            continue
            ;;
        esac
    fi
    rm *2pass*
done


exit 0
