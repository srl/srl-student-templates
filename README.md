SRL Student Templates
=====================

This repository is here to orient the students who conduct a project (Studies on Mechatronics, Bachelor-, Semester-, and Master Theses) at the Soft Robotics Lab (SRL) at ETH Zurich. It contains general information, guidelines, templates and helpful links.

Head over to the [Wiki](https://gitlab.ethz.ch/srl/srl-student-templates/-/wikis/home) for more information or download the entire repository as a ZIP-file [here](https://gitlab.ethz.ch/srl/srl-student-templates/-/archive/master/srl-student-templates-master.zip).

SRL student projects are listed here: [http://www.srl.ethz.ch/education/student-projects.html](http://www.srl.ethz.ch/education/student-projects.html).
 
