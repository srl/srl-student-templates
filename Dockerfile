FROM ubuntu:focal
# skip interactive setup when installing packages
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y && apt-get install -y \
    texlive-latex-recommended \
    texlive-science \
    texlive-latex-extra
